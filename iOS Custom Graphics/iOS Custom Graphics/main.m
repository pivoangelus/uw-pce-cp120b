//
//  main.m
//  iOS Custom Graphics
//
//  Created by Justin Andros on 11/18/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
