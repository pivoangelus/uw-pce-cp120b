//
//  AppDelegate.h
//  iOS Custom Graphics
//
//  Created by Justin Andros on 11/18/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

