//
//  FirstViewController.m
//  iOS Custom Graphics
//
//  Created by Justin Andros on 11/18/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "FirstViewController.h"
#import "VoltagePlotView.h"
#import "WidgetTester.h"

@interface FirstViewController ()
@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.widgetTester performTest];
    [self.voltagePlotView setNeedsDisplay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// testAgain -- a new test performed when the button or graph is pressed.
- (IBAction)testAgain:(id)sender
{
    [self.widgetTester performTest];
    [self.voltagePlotView setNeedsDisplay];
}

@end
