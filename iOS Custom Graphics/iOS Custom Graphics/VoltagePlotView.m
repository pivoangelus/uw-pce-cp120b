//
//  VoltagePlotView.m
//  Widget Plotter
//
//  Created by Justin Andros on 11/17/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "VoltagePlotView.h"
#import "WidgetTester.h"
#import "WidgetTestObservationPoint.h"
#import "loggingMacros.h"

@implementation VoltagePlotView

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *voltagePath = [UIBezierPath bezierPath];
    UIBezierPath *background = [UIBezierPath bezierPathWithRect:self.bounds];
    
    double rectWidth = rect.size.width;
    double rectHeight = rect.size.height;
    // x = observationTime
    // y = voltage
    double xOffset = self.widgetTester.timeMinimum;
    double yOffset = self.widgetTester.sensorMinimum;
    double xRange = (self.widgetTester.sensorMaximum - yOffset) / rectHeight;
    double yRange = (self.widgetTester.timeMaximum - xOffset) / rectWidth;

//    MyLog(@"width: %f, height: %f",rectWidth, rectHeight);
//    MyLog(@"time: start: %f, end: %f", xOffset, self.widgetTester.timeMaximum);
//    MyLog(@"volt: min: %f, max: %f", yOffset, self.widgetTester.sensorMaximum);
    
    CGPoint origin = { .x = 0,          .y = rectHeight };  // bottom left -- flipped origin in iOS
    CGPoint xyMax  = { .x = rectWidth,  .y = rectHeight };  // bottom right -- full length of the xAxis

    [voltagePath moveToPoint:origin];
    for (WidgetTestObservationPoint *observationPoint in self.widgetTester.testData)
    {
        CGPoint plotPoint;
        plotPoint.x = (observationPoint.observationTime - xOffset) / yRange;
        plotPoint.y = (rectHeight - ((observationPoint.voltage - yOffset) / xRange));
        
//        MyLog(@"%@", NSStringFromCGPoint(plotPoint));
        
        [voltagePath addLineToPoint:plotPoint];
    }
    [voltagePath addLineToPoint:xyMax];
    [voltagePath closePath];
    
    switch (self.drawingStyle) {
        // standard -- white background. black line. nothing fancy.
        case drawingStyleStandard:
            [[UIColor whiteColor] setFill];
            [background fill];
            [voltagePath fill];

            [[UIColor blackColor] setStroke];
            [voltagePath stroke];
            break;
        // contrasting colors -- blue background. black line with red fill under the line.
        case drawingStyleContrast:
            [[UIColor blueColor] setFill];
            [background fill];
            
            [[UIColor redColor] setFill];
            [[UIColor blackColor] setStroke];
            [voltagePath fill];
            [voltagePath stroke];
            break;
        // fade -- white background. dashed line with light red and light gray under the line. soft look.
        //         wanted to test dashing line from class, couldn't think of anything fun with it.
        case drawingStyleFade:
            [[UIColor whiteColor] setFill];
            [background fill];
            
            CGFloat dashingArray[2];
            dashingArray[0] = 10.0; // segment painted with stroke color
            dashingArray[1] = 4.0;  // segment no painted with a color
            [voltagePath setLineDash:dashingArray count:6 phase:0];
            
            voltagePath.lineWidth = 3;  // make the dash a little more visible
            [[UIColor colorWithRed:.3 green:.3 blue:.3 alpha:.1] setFill];      // light gray
            [[UIColor colorWithRed:.8 green:.2 blue:.2 alpha:.1] setStroke];    // light red (pinkish)
            [voltagePath fill];
            [voltagePath stroke];
            break;
        // rough -- white background. black line with light gray under the line. testing UIBezierPath flatness.
        case drawingStyleRough:
            [[UIColor whiteColor] setFill];
            [background fill];

            [[UIColor colorWithRed:.3 green:.3 blue:.3 alpha:.1] setFill]; // light gray
            [[UIColor blackColor] setStroke];
            voltagePath.flatness = 30.;
            [voltagePath fill];
            [voltagePath stroke];
            break;
        // WTF -- random color chosen. testing UIBezierPath fillWithBlendMode with color dodge.
        //        for fun, if the UIView is set to clear the graphs will stack on top of each.
        //        WTF came from that interesting development but I felt it may be too confusing.
        case drawingStyleWTF:
        {
            CGFloat red      = ((double)random() / RAND_MAX);
            CGFloat green    = ((double)random() / RAND_MAX);
            CGFloat blue     = ((double)random() / RAND_MAX);
            CGFloat alpha    = ((double)random() / RAND_MAX);
            
            [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
            [background fill];
            
            [[UIColor colorWithRed:red green:green blue:blue alpha:alpha] setFill];
            [[UIColor blackColor] setStroke];
            [voltagePath fillWithBlendMode:kCGBlendModeColorDodge alpha:1.];
            [voltagePath stroke];
            break;
        }
        // negative -- black background with white line.
        case drawingStyleNegative:
            [[UIColor blackColor] setFill];
            [background fill];
            [voltagePath fill];

            [[UIColor whiteColor] setStroke];
            [voltagePath stroke];
            break;
        // should not get here
        default:
            NSLog(@"no color found");
            break;
    }
}

// nextStyle -- finger swipe right to advance forward to the next style
- (IBAction)nextStyle:(id)sender
{
    if (self.drawingStyle < drawingStyleNumberOfStyles - 1)
        self.drawingStyle++;
    else
        self.drawingStyle = 0;
    [self setNeedsDisplay];
}

// previousStyle -- finger swipe left to advance backwards to the next style
- (IBAction)previousStyle:(id)sender
{
    if (self.drawingStyle > 0)
        self.drawingStyle--;
    else
        self.drawingStyle = drawingStyleNumberOfStyles - 1;
    [self setNeedsDisplay];
}

@end
