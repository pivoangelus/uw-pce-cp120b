//
//  SecondViewController.m
//  iOS Custom Graphics
//
//  Created by Justin Andros on 11/18/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
