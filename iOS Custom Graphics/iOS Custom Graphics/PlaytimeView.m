//
//  PlaytimeView.m
//  Widget Plotter
//
//  Created by Justin Andros on 11/17/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "PlaytimeView.h"

@implementation PlaytimeView

- (void)drawRect:(CGRect)rect
{
    CGPoint point =
        {
            .x = self.bounds.origin.x + self.bounds.size.width / 2,
            .y = self.bounds.origin.y + self.bounds.size.height / 2
        };
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextSetFillColorWithColor(context, [UIColor yellowColor].CGColor);
    
    CGRect circle = CGRectMake(point.x / 2.0, point.y - point.x / 2.0, point.x, point.x);
    
    CGContextAddEllipseInRect(context, circle);
    
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGFloat bigRadius = point.x / 2.0;
    CGFloat smallRadius = bigRadius / 4.0;
    
    for (int i = 1; i < 18; i++)
    {
        [PlaytimeView circleRadius:bigRadius smallRadius:smallRadius withCita:i inContext:context withPoint:point];
        bigRadius = point.x / (2 + i);
        smallRadius = bigRadius / (4 + i * 2);
    }
}

+ (void)circleRadius:(CGFloat)bigCircleRadius
         smallRadius:(CGFloat)smallCircleRadius
            withCita:(CGFloat)cita
           inContext:(CGContextRef)context
           withPoint:(CGPoint)point
{
    for (int i = 0; i < 16; i++)
    {
        CGPoint smallCircleCenter = CGPointMake(point.x + bigCircleRadius * cos(cita) - smallCircleRadius / 2.0,
                                                point.y + bigCircleRadius * sin(cita) - smallCircleRadius / 2.0);
        CGRect smallCircleRect = CGRectMake(smallCircleCenter.x,
                                            smallCircleCenter.y,
                                            smallCircleRadius,
                                            smallCircleRadius);
        
        double red      = ((double)random() / RAND_MAX);
        double green    = ((double)random() / RAND_MAX);
        double blue     = ((double)random() / RAND_MAX);
        double alpha    = ((double)random() / RAND_MAX);
        
        UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        
        CGContextSetFillColorWithColor(context, color.CGColor);
        
        CGContextAddEllipseInRect(context, smallCircleRect);
        
        CGContextDrawPath(context, kCGPathFillStroke);
        cita += M_PI / 8.0;
    }
}

@end
