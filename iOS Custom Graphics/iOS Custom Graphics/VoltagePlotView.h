//
//  VoltagePlotView.h
//  Widget Plotter
//
//  Created by Justin Andros on 11/17/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WidgetTester;

typedef NS_ENUM(NSUInteger, DrawingStyle)
    {
        drawingStyleStandard,
        drawingStyleContrast,
        drawingStyleFade,
        drawingStyleRough,
        drawingStyleWTF,
        drawingStyleNegative,
        drawingStyleNumberOfStyles
    };

@interface VoltagePlotView : UIView

@property (strong, nonatomic) IBOutlet WidgetTester *widgetTester;
@property (assign, nonatomic) DrawingStyle drawingStyle;

- (IBAction)nextStyle:(id)sender;
- (IBAction)previousStyle:(id)sender;

@end
