//
//  FirstViewController.h
//  iOS Custom Graphics
//
//  Created by Justin Andros on 11/18/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WidgetTester;
@class VoltagePlotView;

@interface FirstViewController : UIViewController

@property (strong, nonatomic) IBOutlet WidgetTester *widgetTester;
@property (weak, nonatomic) IBOutlet VoltagePlotView *voltagePlotView;

@end
