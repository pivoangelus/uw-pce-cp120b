//
//  Applicant+Human.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/3/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Applicant+Human.h"

@implementation Applicant (Human)

+ (Applicant *)findOrCreateApplicantWithName:(NSString *) searchName
                                     context:(NSManagedObjectContext *)moc;
{
    NSArray *applicant = [self applicantsWithName:searchName context:moc];
    Applicant *result = [applicant lastObject];
    
    if (!result)
    {
        result = [NSEntityDescription insertNewObjectForEntityForName:@"Applicant"
                                               inManagedObjectContext:moc];
        result.name = searchName;
    }

    return result;
}

+ (NSArray *)applicantsWithName:(NSString *)searchName
                        context:(NSManagedObjectContext *)moc;
{
    NSManagedObjectModel *mom = moc.persistentStoreCoordinator.managedObjectModel;
    NSFetchRequest *nameFetchRequest = [mom fetchRequestFromTemplateWithName:@"applicantsByName"
                                                       substitutionVariables:@{@"nameToken" : searchName }];
    NSError *error;
    NSArray *result = [moc executeFetchRequest:nameFetchRequest error:&error];
    
    if (!result)
        NSLog(@"applicants: %@", error.localizedDescription);
    return result;
}

@end
