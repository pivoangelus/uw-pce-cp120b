//
//  Property.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/8/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Property.h"
#import "Application.h"


@implementation Property

@dynamic address;
@dynamic latitude;
@dynamic longitude;
@dynamic application;

@end
