//
//  Application+Human.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/3/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Application.h"

@interface Application (Human)

- (NSString *)stringForGoogleMapURL;
+ (NSString *)stringForJSONURL;

+ (BOOL)repopulateFromScratch:(NSManagedObjectContext *)moc;
+ (BOOL)repopulateFromScratchWithData:(NSData *)inputData
                              context:(NSManagedObjectContext *)moc;

+ (Application *)findOrCreateApplicationWithPermitNumber:(NSString *)searchName
                                                 context:(NSManagedObjectContext *)moc;
+ (NSArray *)applicationsWithPermitNumber:(NSString *)searchName
                                  context:(NSManagedObjectContext *)moc;

@end
