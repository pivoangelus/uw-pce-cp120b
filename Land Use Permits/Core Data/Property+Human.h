//
//  Property+Human.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/3/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Property.h"

@interface Property (Human)

+ (Property *)findOrCreatePropertyWithAddress:(NSString *)searchName
                                      context:(NSManagedObjectContext *)moc;
+ (NSArray *)propertiesWithAddress:(NSString *)searchName
                           context:(NSManagedObjectContext *)moc;

@end
