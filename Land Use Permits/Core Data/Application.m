//
//  Application.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/8/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Application.h"
#import "Applicant.h"
#import "Property.h"


@implementation Application

@dynamic applicationDate;
@dynamic category;
@dynamic decisionDate;
@dynamic descr;
@dynamic issueDate;
@dynamic permitNumber;
@dynamic status;
@dynamic type;
@dynamic url;
@dynamic value;
@dynamic applicant;
@dynamic property;

@end
