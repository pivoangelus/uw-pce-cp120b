//
//  Applicant+Human.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/3/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Applicant.h"

@interface Applicant (Human)

+ (Applicant *)findOrCreateApplicantWithName:(NSString *) searchName
                                     context:(NSManagedObjectContext *)moc;
+ (NSArray *)applicantsWithName:(NSString *)searchName
                        context:(NSManagedObjectContext *)moc;

@end
