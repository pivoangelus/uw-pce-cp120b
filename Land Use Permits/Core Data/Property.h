//
//  Property.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/8/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Application;

@interface Property : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSSet *application;
@end

@interface Property (CoreDataGeneratedAccessors)

- (void)addApplicationObject:(Application *)value;
- (void)removeApplicationObject:(Application *)value;
- (void)addApplication:(NSSet *)values;
- (void)removeApplication:(NSSet *)values;

@end
