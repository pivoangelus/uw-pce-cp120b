//
//  Application+Human.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/3/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Application+Human.h"
#import "Applicant+Human.h"
#import "Property+Human.h"

@implementation Application (Human)

+ (Application *)findOrCreateApplicationWithPermitNumber:(NSString *)searchName
                                                 context:(NSManagedObjectContext *)moc;
{
    NSArray *apps = [self applicationsWithPermitNumber:searchName context:moc];
    Application *result = [apps lastObject];
    
    if (!result)
    {
        result = [NSEntityDescription insertNewObjectForEntityForName:@"Application"
                                               inManagedObjectContext:moc];
        result.permitNumber = searchName;
    }
    return result;
}

+ (NSArray *)applicationsWithPermitNumber:(NSString *)searchName
                                  context:(NSManagedObjectContext *)moc;
{
    NSManagedObjectModel *mom = moc.persistentStoreCoordinator.managedObjectModel;
    
    NSFetchRequest *permitNumberFetchRequest =
    [mom fetchRequestFromTemplateWithName:@"applicationsByPermitNumber"
                    substitutionVariables:@{@"permitNumberToken" : searchName}];
    NSError *error;
    NSArray *result = [moc executeFetchRequest:permitNumberFetchRequest error:&error];
    if (!result)
        NSLog(@"application: %@", error.localizedDescription);
    return result;
    
}

+ (NSString *)stringForJSONURL
{
    return @"https://data.seattle.gov/api/views/uyyd-8gak/rows.json?accessType=DOWNLOAD";
}

- (NSString *)stringForGoogleMapURL
{
    NSString *latitude = self.property.latitude.stringValue;
    NSString *longitude = self.property.longitude.stringValue;
    
    //https://maps.googleapis.com/maps/api/staticmap?center=xxxx,xxxx8&zoom=11&size=200x200&markers=color:red%7Cxxxx,xxxx
    NSString *string = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@", @"https://maps.googleapis.com/maps/api/staticmap?center=", latitude, @",", longitude, @"&zoom=15&size=400x400&markers=color:red%7C", latitude, @",", longitude];
    
    return string;
}

+ (void)exploreDatabase:(NSDictionary *)database
{
    NSArray *data = database[@"data"];
    NSDictionary *meta = database[@"meta"];
    NSLog(@"'meta' keys: %@",meta.allKeys);
    NSDictionary *view = meta[@"view"];
    NSLog(@"'view' keys: %@",view.allKeys);
    //NSObject *columns = view[@"columns"];
    NSArray *columns = view[@"columns"];
    NSLog(@"columns are of class %@",columns.class);
    NSLog(@"column elements are of class %@", [columns[0] class]);
    NSDictionary *aColumnsElement = columns[0];
    NSLog(@"columns keys are %@", aColumnsElement.allKeys);
    
    int columnCount = 0;
    NSLog(@"column values: position name description dataTypeName fieldName");
    for (NSDictionary *obj in columns)
    {
        columnCount++;
        //NSLog(@"column %d: %@", columnCount, obj);
        NSLog(@"column %d: %@ %@ %@ %@ %@", columnCount, obj[@"position"], obj[@"name"], obj[@"description"], obj[@"dataTypeName"], obj[@"fieldName"]);
    }
    NSLog(@"meta %@ %@", meta.class, meta.allKeys);
    NSLog(@"view %@ %@", view.class, view.allKeys);
    
    int j = 0;
    for (NSArray *row in data)
    {
        int i = 0;
        for (id col in row)
        {
            NSLog(@"%d %@", i++, col);
        }
        // replace to when row == null
        if (j++ > 20)
            break;
    }
}

+ (BOOL)repopulateFromScratch:(NSManagedObjectContext *)moc;
{
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[self stringForJSONURL]]];
    
    return [self repopulateFromScratchWithData:data context:moc];
}

+ (BOOL)repopulateFromScratchWithData:(NSData *)inputData
                              context:(NSManagedObjectContext *)moc;
{
    NSLog(@"parsing data of size %ld bytes", inputData.length);
    NSDictionary *database;
    NSError *error;
    
    database = [NSJSONSerialization JSONObjectWithData:inputData options:kNilOptions error:&error];
    if (!database)
    {
        NSLog(@"%@ %@", error.localizedDescription, error.localizedFailureReason);
        return NO;
    }
    else
    {
        //[self exploreDatabase:database];
        //NSLog(@"data downloaded");
        
        NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        
        rfc3339DateFormatter.locale = enUSPOSIXLocale;
        rfc3339DateFormatter.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
        rfc3339DateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        
        NSUInteger batchCount = 1;
        for (NSArray *row in database[@"data"])
        {
            @autoreleasepool
            {
                NSString *permitNumberString = nil;
                NSString *addressString = nil;
                NSString *applicantString = nil;
                
                if (row[8] != [NSNull null])
                    permitNumberString = row[8];
                
                if (row[10] != [NSNull null])
                    addressString = row[10];
                
                if (row[16] != [NSNull null])
                    applicantString = row[16];
                
                Application *application =
                    [Application findOrCreateApplicationWithPermitNumber:permitNumberString
                                                                 context:moc];
                if (row[9] != [NSNull null])
                    application.type = row[9];
                
                if (row[11] != [NSNull null])
                    application.descr = row[11];
                
                if (row[12] != [NSNull null])
                    application.category = row[12];
                
                if (row[15] != [NSNull null])
                    application.value = @([row[15] doubleValue]);
                
                if (row[17] != [NSNull null])
                {
                    NSString *applicationDateString = row[17];
                    application.applicationDate = [rfc3339DateFormatter dateFromString:applicationDateString];
                }
                
                if (row[18] != [NSNull null])
                {
                    NSString *decisionDateString = row[18];
                    application.decisionDate = [rfc3339DateFormatter dateFromString:decisionDateString];
                }
                
                if (row[20] != [NSNull null])
                {
                    NSString *issueDateString = row[20];
                    application.issueDate = [rfc3339DateFormatter dateFromString:issueDateString];
                }
                
                if (row[21] != [NSNull null])
                    application.status = row[21];
                
                if (row[23] != [NSNull null])
                    application.url = row[23][0];
                
                application.permitNumber = permitNumberString;
                
                if (applicantString)
                {
                    Applicant *applicant = [Applicant findOrCreateApplicantWithName:applicantString
                                                                            context:moc];
                    application.applicant = applicant;
                }
                
                if (addressString)
                {
                    Property *property = [Property findOrCreatePropertyWithAddress:addressString
                                                                           context:moc];
                    
                    if (row[24] != [NSNull null])
                    {
                        NSString *latitudeString = row[24];
                        property.latitude = @([latitudeString doubleValue]);
                    }
                    
                    if (row[25] != [NSNull null])
                    {
                        NSString *longitudeString = row[25];
                        property.longitude = @([longitudeString doubleValue]);
                    }
                                    
                    application.property = property;
                }
                // keep the save!
                batchCount++;
                // sync kept crashing so instead of @try/@catch i just test to see if there is a private queue
                // need to look around for a better method to this if perform two styles of sync
                if (moc.concurrencyType == NSPrivateQueueConcurrencyType)
                {
                    if (batchCount > 100)
                    {
                        NSError *savedError;
                        if (![moc save:&savedError])
                            NSLog(@"%@ %@", application, error.localizedDescription);
                        batchCount = 1;
                    }
                }
            }
        }
    }
    // save the final pieces that could be lost!
    // also don't allow the sync method to try and save, silly main
    if (moc.concurrencyType == NSPrivateQueueConcurrencyType)
    {
        NSError *savedError;
        [moc save:&savedError];
    }
    return YES;
}

@end
