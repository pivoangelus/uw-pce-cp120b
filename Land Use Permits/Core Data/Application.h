//
//  Application.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/8/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Applicant, Property;

@interface Application : NSManagedObject

@property (nonatomic, retain) NSDate * applicationDate;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * decisionDate;
@property (nonatomic, retain) NSString * descr;
@property (nonatomic, retain) NSDate * issueDate;
@property (nonatomic, retain) NSString * permitNumber;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) Applicant *applicant;
@property (nonatomic, retain) Property *property;

@end
