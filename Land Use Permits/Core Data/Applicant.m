//
//  Applicant.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/8/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Applicant.h"
#import "Application.h"


@implementation Applicant

@dynamic name;
@dynamic application;

@end
