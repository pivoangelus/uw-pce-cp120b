//
//  Property+Human.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/3/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Property+Human.h"

@implementation Property (Human)

+ (Property *)findOrCreatePropertyWithAddress:(NSString *)searchName
                                      context:(NSManagedObjectContext *)moc;
{
    NSArray *properties = [self propertiesWithAddress:searchName context:moc];
    Property *result = [properties lastObject];
    
    if (!result)
    {
        result = [NSEntityDescription insertNewObjectForEntityForName:@"Property"
                                               inManagedObjectContext:moc];
        result.address = searchName;
    }

    return result;
}

+ (NSArray *)propertiesWithAddress:(NSString *)searchName
                           context:(NSManagedObjectContext *)moc;
{
    NSManagedObjectModel *mom = moc.persistentStoreCoordinator.managedObjectModel;
    NSFetchRequest *addressFetchRequest = [mom fetchRequestFromTemplateWithName:@"propertiesByAddress"
                                                          substitutionVariables:@{@"addressToken" : searchName }];
    NSError *error;
    NSArray *result = [moc executeFetchRequest:addressFetchRequest error:&error];
    
    if (!result)
        NSLog(@"property: %@", error.localizedDescription);
    return result;
}

@end
