//
//  AppDelegate.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/2/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end

