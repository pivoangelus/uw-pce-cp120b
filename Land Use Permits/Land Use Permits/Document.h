//
//  Document.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/2/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class LandUsePermitLoader;

@interface Document : NSPersistentDocument

@property (nonatomic, retain) NSNumber *index;
@property (weak) IBOutlet NSTableView *applicationTable;
@property (nonatomic, retain) LandUsePermitLoader *applicationLoader;
@property (weak) IBOutlet NSButton *fetchItBttn;
@property (weak) IBOutlet NSButton *fetchAsyncBttn;

@end
