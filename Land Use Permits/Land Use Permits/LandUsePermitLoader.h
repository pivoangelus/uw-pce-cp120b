//
//  LandUsePermitLoader.h
//  Land Use Permits
//
//  Created by Justin Andros on 12/6/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LandUsePermitLoader : NSObject <NSURLConnectionDataDelegate>

@property (strong, nonatomic) NSMutableData *incomingData;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSManagedObjectContext *moc;

- (void)downloadAndParseWithContext:(NSManagedObjectContext *)context;

@end
