//
//  LandUsePermitLoader.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/6/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "LandUsePermitLoader.h"
#import "Application+Human.h"

@implementation LandUsePermitLoader

- (instancetype)init
{
    if (self = [super init])
    {
        self.incomingData = [NSMutableData data];
    }
    return self;
}

- (void)downloadAndParseWithContext:(NSManagedObjectContext *)context
{
    self.moc = context;
    NSURL *url = [NSURL URLWithString:[Application stringForJSONURL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

#pragma - mark delegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.incomingData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSManagedObjectContext *childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    childContext.parentContext = self.moc;
    
    __block BOOL parseResult;
    [childContext performBlock:^{
        parseResult = [Application repopulateFromScratchWithData:self.incomingData
                                                         context:childContext];
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
}

@end
