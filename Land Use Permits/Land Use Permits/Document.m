//
//  Document.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/2/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Document.h"
#import "Application+Human.h"
#import "LandUsePermitLoader.h"

@interface Document ()

@end

@implementation Document

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController {
    [super windowControllerDidLoadNib:aController];
    
    NSPersistentStoreCoordinator *psc = self.managedObjectContext.persistentStoreCoordinator;
    NSManagedObjectContext *newMOC =
    [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    newMOC.persistentStoreCoordinator = psc;
    self.managedObjectContext = newMOC;
    
    [self indexUpdate];
}

+ (BOOL)autosavesInPlace {
    return YES;
}

- (NSString *)windowNibName {
    return @"Document";
}

// you get one chance at the sync buttons. don't have a reset implemented yet.
- (IBAction)fetchIt:(id)sender
{
    [Application repopulateFromScratch:self.managedObjectContext];
    [self.fetchItBttn setEnabled:NO];
    [self.fetchAsyncBttn setEnabled:NO];
}

- (IBAction)fetchAsync:(id)sender
{
    //NSLog(@"fetch async");
    self.applicationLoader = [[LandUsePermitLoader alloc] init];
    [self.applicationLoader downloadAndParseWithContext:self.managedObjectContext];
    [self.fetchItBttn setEnabled:NO];
    [self.fetchAsyncBttn setEnabled:NO];
}

- (void)showStatusInBrowser:(NSArray *)targets
{
    for (Application *app in targets)
    {
        //NSLog(@"%@",app.url);
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:app.url]];
    }
}

- (void)showMapInBrowser:(NSArray *)targets
{
    for (Application *app in targets)
    {
        //NSLog(@"%@",app.stringForGoogleMapURL);
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:app.stringForGoogleMapURL]];
    }
}

// didn't want the user to select more then one and show where you were in the table instead
// this for some reason was annoying to setup
- (void)indexUpdate
{
    double i = self.applicationTable.selectedRow;
    if (i != -1)
    {
        i += 1.;
        self.index = @(i);
        //NSLog(@"%@",self.index);
    }
    else
        self.index = @(1);
}

- (IBAction)applicationTableviewClicked:(NSTableView *)sender
{
    [self indexUpdate];
}

@end
