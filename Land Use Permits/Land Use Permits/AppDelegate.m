//
//  AppDelegate.m
//  Land Use Permits
//
//  Created by Justin Andros on 12/2/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
}

@end
