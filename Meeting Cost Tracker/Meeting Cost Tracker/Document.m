//
//  Document.m
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Document.h"
#import "Meeting.h"
#import "Person.h"
#import "loggingMacros.h"

NSString *tableOfParticipantsDefaultBackgroundColorKey = @"theColor";

@interface Document ()

- (void)setMeeting:(Meeting *)aMeeting;
- (NSTimer *)timer;
- (void)setTimer:(NSTimer *)aTimer;
- (void)updateGUI:(NSTimer *)aTimer;

@end

@implementation Document

- (instancetype)init
{
    if (self = [super init])
    {
        _meeting = [[Meeting alloc] init];
        [self startObservingMeeting:_meeting];
    }
    return self;
}

- (void)dealloc
{
    [self stopObservingMeeting:_meeting];
    [_meeting release], _meeting = nil;
    [_timer invalidate], [_timer release], _timer = nil;
    [super dealloc];
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    
    // constant update of date, time using NSTimer
    // time interval (secs), target document open, method used at each fire, extra info (unused), repeat
    [self setTimer:
     [NSTimer scheduledTimerWithTimeInterval:0.2
                                      target:self
                                    selector:@selector(updateGUI:)
                                    userInfo:nil
                                     repeats:YES]];
    
    // sets up a binding for the NSTableView background color
    [[self tableOfParticipants] bind:@"backgroundColor"
                            toObject:[NSUserDefaultsController sharedUserDefaultsController]
                         withKeyPath:@"values.theColor"
                             options:[NSDictionary dictionaryWithObject:NSUnarchiveFromDataTransformerName forKey:NSValueTransformerNameBindingOption]];
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

- (NSString *)windowNibName
{
    return @"Document";
}

// archive our document
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    if (outError != NULL)
    {
        *outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:NULL];
    }
    return [NSKeyedArchiver archivedDataWithRootObject:self.meeting];
}

// open our saved document
- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    Meeting *newMeeting;
    @try
    {
        newMeeting = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    @catch (NSException *exception)
    {
        if (outError)
        {
            NSDictionary *d = [NSDictionary dictionaryWithObject:@"Data is corrupted."
                                                          forKey:NSLocalizedFailureReasonErrorKey];
            *outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:d];
        }
        return NO;
    }
    self.meeting = newMeeting;
    return YES;
}

// _meeting getter and setter
- (Meeting *)meeting
{
    return _meeting;
}

- (void)setMeeting:(Meeting *)aMeeting
{
    if (_meeting != aMeeting)
    {
        [self stopObservingMeeting:_meeting];
        [_meeting release];
        _meeting = [aMeeting retain];
        [self startObservingMeeting:_meeting];
    }
}

// _timer getter and setter
- (NSTimer *)timer
{
    return _timer;
}

- (void)setTimer:(NSTimer *)aTimer
{
    if (_timer != aTimer)
    {   // need to make sure to invalidate _and_ release
        [_timer invalidate];
        [_timer release];
        _timer = [aTimer retain];
    }
}

- (void)windowWillClose:(NSNotification *)aNotification
{   // stop NSTimer from firing and crashing a Document that is closing
    [self setTimer:nil];
}

- (void)updateGUI:(NSTimer *)aTimer
{   // update current date label, elapsed time label, and accured cost label
    [[self timeStamp] setObjectValue:[NSDate date]];
    [[self elapsedTimeLabel] setStringValue:[[self meeting] elapsedTimeDisplayString]];
    [[self accuredCostLabel] setObjectValue:[[self meeting] accruedCost]];
}

- (NSColor *)defaultBackgroundColorNSTableView
{
    NSColor *defaultColor = nil;
    NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:tableOfParticipantsDefaultBackgroundColorKey];
    
    if (colorData != nil)
        defaultColor = (NSColor *)[NSUnarchiver unarchiveObjectWithData:colorData];
    
    return defaultColor;
}

- (IBAction)startMeeting:(id)sender
{   // set starting Time var, bindings will update
    [[self meeting] setStartingTime:[NSDate date]];
    
    // ensure the meeting starts before it even ended
    [[self meeting] setEndingTime:nil];
}

- (IBAction)endMeeting:(id)sender
{   // set ending Time var, bindings will update
    [[self meeting] setEndingTime:[NSDate date]];
}

- (IBAction)logMeeting:(id)sender
{   // echo meeting object
    NSLog(@"Starting Time: %@\nEnding Time: %@", [[self meeting] startingTime], [[self meeting] endingTime]);
}

- (IBAction)logParticipants:(id)sender
{   // echo persons at the meeting
    NSLog(@"Participants: %@", [[self meeting] personsPresent]);
}

- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{   // enable/disbale start and end meeting menu items
    SEL theAction = [anItem action];
    
    // attach canStart, canStop to the menu items
    if (theAction == @selector(startMeeting:))
        return self.meeting.canStart;
    else if (theAction == @selector(endMeeting:))
        return self.meeting.canStop;
    
    // Subclass of NSDocument, so invoke super's implementation
    return [super validateUserInterfaceItem:anItem];
}

- (IBAction)resetWithCaptains:(id)sender
{
    self.meeting = [Meeting meetingWithCaptains];
}

- (IBAction)resetWithMarxBrothers:(id)sender
{
    self.meeting = [Meeting meetingWithMarxBrothers];
}

- (IBAction)resetWithStooges:(id)sender
{
    self.meeting = [Meeting meetingWithStooges];
}

#pragma mark - KVO

- (void)startObservingMeeting:(Meeting *)aMeeting
{
    [aMeeting addObserver:self
               forKeyPath:personsPresentKeyPath
                  options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                  context:NULL];
    for (Person *p in aMeeting.personsPresent)
        [self startObservingPerson:p];
}

- (void)stopObservingMeeting:(Meeting *)aMeeting
{
    [aMeeting removeObserver:self forKeyPath:personsPresentKeyPath];
    for (Person *p in aMeeting.personsPresent)
        [self stopObservingPerson:p];
}

- (void)startObservingPerson:(Person *)aPerson
{
    [aPerson addObserver:self
              forKeyPath:nameKeyPath
                 options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                 context:NULL];
    [aPerson addObserver:self
              forKeyPath:hourlyRateKeyPath
                 options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                 context:NULL];
}

- (void)stopObservingPerson:(Person *)aPerson
{
    [aPerson removeObserver:self forKeyPath:nameKeyPath];
    [aPerson removeObserver:self forKeyPath:hourlyRateKeyPath];
}

- (void)changeKeyPath:(NSString *)keyPath
             ofObject:(id)obj
              toValue:(id)newValue
{
    // setValue:forKeyPath: will cause the key-value observing method
    // to be called, which takes care of the undo stuff
    [obj setValue:newValue forKeyPath:keyPath];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
//    LogMethod();
//    NSLog(@"Document OVFKP\nKEYPATH: %@\nCHANGE:\n%@\nKINDKEY: %@", keyPath, change, change[NSKeyValueChangeKindKey]);
    
    switch ([change[NSKeyValueChangeKindKey] integerValue])
    {
        case NSKeyValueChangeSetting:
        {
            id oldValue = [change objectForKey:NSKeyValueChangeOldKey];
            
            if (oldValue == [NSNull null]) oldValue = nil;
            
            [[self.undoManager prepareWithInvocationTarget:self] changeKeyPath:keyPath
                                                                      ofObject:object
                                                                       toValue:oldValue];
            break;
        }
        case NSKeyValueChangeInsertion:
//            MyLog(@"Insertion: %@ %@",change[NSKeyValueChangeNewKey], change[NSKeyValueChangeIndexesKey]);
            [[self.undoManager prepareWithInvocationTarget:self.meeting] removePersonsPresentAtIndexes:change[NSKeyValueChangeIndexesKey]];
            for (Person *p in change[NSKeyValueChangeNewKey])
                [self startObservingPerson:p];
            break;
        case NSKeyValueChangeRemoval:
//            MyLog(@"Removal: %@ %@", change[NSKeyValueChangeOldKey], change[NSKeyValueChangeIndexesKey]);
            [[self.undoManager prepareWithInvocationTarget:self.meeting] insertPersonsPresent:change[NSKeyValueChangeOldKey]
                                                                                    atIndexes:change[NSKeyValueChangeIndexesKey]];
            for (Person *p in change[NSKeyValueChangeOldKey])
                [self stopObservingPerson:p];
            break;
        case NSKeyValueChangeReplacement:
            break;
    }
}

@end
