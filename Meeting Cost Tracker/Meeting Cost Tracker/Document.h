//
//  Document.h
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Meeting.h"

extern NSString *tableOfParticipantsDefaultBackgroundColorKey;

@interface Document : NSDocument
{
    Meeting *_meeting;
    NSTimer *_timer;   
}

- (Meeting *)meeting;

// Labels used for time and costs
@property (assign) IBOutlet NSTextField *timeStamp;
@property (assign) IBOutlet NSTextField *startTimeLabel;
@property (assign) IBOutlet NSTextField *endTimeLabel;
@property (assign) IBOutlet NSTextField *elapsedTimeLabel;
@property (assign) IBOutlet NSTextField *accuredCostLabel;
@property (assign) IBOutlet NSTextField *totalBillingRateKVOLabel;
@property (assign) IBOutlet NSTextField *totalBillingRateTargetAction;

// Table of participants (personsPresent: name, hourlyRate)
@property (assign) IBOutlet NSTableView *tableOfParticipants;

// To turn on and off the buttons
@property (assign) IBOutlet NSButton *startMeetingButton;
@property (assign) IBOutlet NSButton *endMeetingButton;

// Button actions
- (IBAction)startMeeting:(id)sender;
- (IBAction)endMeeting:(id)sender;
- (IBAction)logMeeting:(id)sender;
- (IBAction)logParticipants:(id)sender;

// Menu reset items
- (IBAction)resetWithCaptains:(id)sender;
- (IBAction)resetWithMarxBrothers:(id)sender;
- (IBAction)resetWithStooges:(id)sender;

@end

