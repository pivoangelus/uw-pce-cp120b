//
//  PreferencesController.h
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 11/7/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PreferencesController : NSWindowController

@property (assign) IBOutlet NSTextField *defaultName;
@property (assign) IBOutlet NSTextField *defaultHourlyBillingRate;
@property (assign) IBOutlet NSColorWell *defaultColorWell;

- (IBAction)resetDefaults:(id)sender;
- (IBAction)defaultColorSelection:(id)sender;

@end
