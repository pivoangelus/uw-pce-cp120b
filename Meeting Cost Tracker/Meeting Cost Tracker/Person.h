//
//  Person.h
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *personsCreatedCounterKey;
extern NSString *personDefaultNameKey;
extern NSString *personDefaultHourlyRateKey;

extern NSString *nameKeyPath;
extern NSString *hourlyRateKeyPath;

@interface Person : NSObject
{
    NSString *_name;
    NSNumber *_hourlyRate;
}

// archiving
- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

// getter and setter
- (NSString *)name;
- (void)setName:(NSString *)aParticipantName;
- (NSNumber *)hourlyRate;
- (void)setHourlyRate:(NSNumber *)anHourlyRate;

+ (Person *)personWithName:(NSString *)name
                hourlyRate:(NSNumber *)rate;
- (id)initWithName:(NSString*)aParticipantName
              rate:(NSNumber *)aRate;

@end
