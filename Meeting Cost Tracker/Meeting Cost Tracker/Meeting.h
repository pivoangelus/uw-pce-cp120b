//
//  Meeting.h
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *personsPresentKeyPath;
extern NSString *startingTimeKeyPath;
extern NSString *endingTimeKeyPath;

@interface Meeting : NSObject
{
    NSDate *_startingTime;
    NSDate *_endingTime;
    
    NSMutableArray *_personsPresent;
}

// archiving
- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

// getter and setter
- (NSDate *)startingTime;
- (void)setStartingTime:(NSDate *)aStartingTime;
- (NSDate *)endingTime;
- (void)setEndingTime:(NSDate *)anEndingTime;
- (NSMutableArray *)personsPresent;
- (void)setPersonsPresent:(NSMutableArray *)aPersonsPresent;

- (void)addToPersonsPresent:(id)personsPresentObject;
- (void)removeFromPersonsPresent:(id)personsPresentObject;
- (void)removeObjectFromPersonsPresentAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inPersonsPresentAtIndex:(NSUInteger)idx;
- (void)insertPersonsPresent:(NSArray *)aPerson atIndexes:(NSIndexSet *)idx;
- (void)removePersonsPresentAtIndexes:(NSIndexSet *)idx;
- (NSUInteger)countOfPersonsPresent;

- (NSUInteger)elapsedSeconds;
- (double)elapsedHours;
- (NSString *)elapsedTimeDisplayString;

- (NSNumber *)accruedCost;
- (NSNumber *)totalBillingRate;

- (BOOL)canStart;
- (BOOL)canStop;

+ (Meeting *)meetingWithStooges;
+ (Meeting *)meetingWithCaptains;
+ (Meeting *)meetingWithMarxBrothers;

@end
