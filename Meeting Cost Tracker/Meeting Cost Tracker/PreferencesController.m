//
//  PreferencesController.m
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 11/7/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "PreferencesController.h"
#import "Person.h"
#import "Document.h"

// defaults
NSString *defaultName           = @"Frankenstein";
NSString *defaultHourlyRate     = @"100.";
NSString *defaultCreatedCounter = @"0";

@interface PreferencesController ()
@end

@implementation PreferencesController

- (instancetype)init
{
    if (self = [super initWithWindowNibName:@"Preferences"])
    {
        [self registerStandardDefaults];
    }
    return self;
}

- (void)registerStandardDefaults
{
    [self registerStandardDefaultsWithColor:[NSColor whiteColor]];
}

- (void)registerStandardDefaultsWithColor:(NSColor *)aColor
{
    NSMutableDictionary *defaultValues = [NSMutableDictionary dictionary];
    NSData *colorData = [NSArchiver archivedDataWithRootObject:aColor];
    
    defaultValues[personDefaultNameKey] = defaultName;
    defaultValues[personDefaultHourlyRateKey] = [NSNumber numberWithDouble:defaultHourlyRate.doubleValue];
    defaultValues[personsCreatedCounterKey] = [NSNumber numberWithDouble:defaultCreatedCounter.doubleValue];
    defaultValues[tableOfParticipantsDefaultBackgroundColorKey] = colorData;
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
}

- (IBAction)resetDefaults:(id)sender
{
    NSDictionary *defaultsDict = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    for (NSString *key in [defaultsDict allKeys])
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [self registerStandardDefaults];
}

- (IBAction)defaultColorSelection:(id)sender
{   // NSColorWell selection
    NSColor *aColor = [[self defaultColorWell] color];
    [self registerStandardDefaultsWithColor:aColor];
}

-(void)showWindow:(id)sender
{   // start tracking the preferences window
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(windowWillClose)
                                                 name:NSWindowWillCloseNotification
                                               object:NULL];
    [super showWindow:sender];
}

-(void)windowWillClose
{   // once the window closes set the text field items to the defaults
    // this is done if the user forgets to hit enter when entering the new values (quick solution)
    // NSColorWell automatically updates. Note to self, need to figure out how to use KVO to do this?
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    NSNumber *num = [NSNumber numberWithDouble:[[self defaultHourlyBillingRate] doubleValue]];
    [[NSUserDefaults standardUserDefaults] setObject:[[self defaultName] stringValue] forKey:personDefaultNameKey];
    [[NSUserDefaults standardUserDefaults] setObject:num forKey:personDefaultHourlyRateKey];
}

@end
