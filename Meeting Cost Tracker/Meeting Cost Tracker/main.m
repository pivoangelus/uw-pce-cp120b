//
//  main.m
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
