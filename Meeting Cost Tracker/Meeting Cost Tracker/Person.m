//
//  Person.m
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Person.h"

@implementation Person

NSString *personsCreatedCounterKey   = @"personsCreatedCounter";
NSString *personDefaultNameKey       = @"defaultName";
NSString *personDefaultHourlyRateKey = @"defaultHourlyRate";

NSString *nameKeyPath       = @"name";
NSString *hourlyRateKeyPath = @"hourlyRate";

- (instancetype)init
{
    NSInteger personCounter = [[NSUserDefaults standardUserDefaults] integerForKey:personsCreatedCounterKey];
    [[NSUserDefaults standardUserDefaults] setInteger:personCounter+1
                                               forKey:personsCreatedCounterKey];

    return [self initWithName:[NSString stringWithFormat:@"%@ #%ld",
                               [[NSUserDefaults standardUserDefaults] valueForKey:personDefaultNameKey], personCounter]
                         rate:[[NSUserDefaults standardUserDefaults] objectForKey:personDefaultHourlyRateKey]];
}

- (instancetype)initWithName:(NSString *)aParticipantName
              rate:(NSNumber *)aRate
{
    if (self = [super init])
    {
        _name = [aParticipantName copy];
        _hourlyRate = [aRate retain];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        _name = [[decoder decodeObjectForKey:nameKeyPath] retain];
        _hourlyRate = [[decoder decodeObjectForKey:hourlyRateKeyPath] retain];
    }
    return self;
}

- (void)dealloc
{
    [_hourlyRate release], _hourlyRate = nil;
    [_name release], _name = nil;
    [super dealloc];
}

- (NSString *)description
{   // overriding description
    return [NSString stringWithFormat:@"<Person %@ %@>", [self name], [self hourlyRate]];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:[self hourlyRate] forKey:hourlyRateKeyPath];
    [encoder encodeObject:[self name] forKey:nameKeyPath];
}

// _name getter and setter
- (NSString *)name
{
    return _name;
}

- (void)setName:(NSString *)aParticipantName
{
    if (_name != aParticipantName)
    {
        [_name release];
        _name = [aParticipantName copy];
    }
}

// _hourlyRate getter and setter
- (NSNumber *)hourlyRate
{
    return _hourlyRate;
}

- (void)setHourlyRate:(NSNumber *)anHourlyRate
{
    if (_hourlyRate != anHourlyRate)
    {
        [_hourlyRate release];
        _hourlyRate = [anHourlyRate retain];
    }
}

+ (Person *)personWithName:(NSString *)aName
                hourlyRate:(NSNumber *)aRate
{   // allows us to create a Person object, release it and save some writing
    return [[[Person alloc] initWithName:aName rate:aRate] autorelease];
}

@end
