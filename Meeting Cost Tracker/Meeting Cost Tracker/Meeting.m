//
//  Meeting.m
//  Meeting Cost Tracker
//
//  Created by Justin Andros on 10/15/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Meeting.h"
#import "Person.h"
#import "loggingMacros.h"

@implementation Meeting

// keyPaths
NSString *personsPresentKeyPath = @"personsPresent";
NSString *startingTimeKeyPath   = @"startingTime";
NSString *endingTimeKeyPath     = @"endingTime";

- (instancetype)init
{
    if (self = [super init])
    {
        _personsPresent = [[NSMutableArray array] retain];
        // add an observer to personsPresent, watch for any changes
        [self addObserver:self
               forKeyPath:personsPresentKeyPath
                  options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                  context:NULL];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {   // reset all the vars from the archive
        _personsPresent = [[decoder decodeObjectForKey:personsPresentKeyPath] retain];
        _startingTime = [[decoder decodeObjectForKey:startingTimeKeyPath] retain];
        _endingTime = [[decoder decodeObjectForKey:endingTimeKeyPath] retain];
        
        // place the observer back onto personsPresent and each hourlyRate
        [self addObserver:self
               forKeyPath:personsPresentKeyPath
                  options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                  context:NULL];
        [self startObservingPersonsPresent];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:personsPresentKeyPath];
    [self stopObservingPersonsPresent];
    
    [_startingTime release], _startingTime = nil;
    [_endingTime release], _endingTime = nil;
    [_personsPresent release], _personsPresent = nil;
    [super dealloc];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{   // archive
    [encoder encodeObject:[self personsPresent] forKey:personsPresentKeyPath];
    [encoder encodeObject:[self startingTime] forKey:startingTimeKeyPath];
    [encoder encodeObject:[self endingTime] forKey:endingTimeKeyPath];
}

- (NSString *)description
{   // overriding description
    return [NSString stringWithFormat:@"<Meeting %@ %@ %@>", [self startingTime], [self endingTime], [self personsPresent]];
}

- (NSDate *)startingTime
{
    return _startingTime;
}

- (void)setStartingTime:(NSDate *)aStartingTime
{
    if (_startingTime != aStartingTime)
    {
        [_startingTime release];
        _startingTime = [aStartingTime retain];
    }
}

- (NSDate *)endingTime
{
    return _endingTime;
}

- (void)setEndingTime:(NSDate *)anEndingTime
{
    if (_endingTime != anEndingTime)
    {
        [_endingTime release];
        _endingTime = [anEndingTime retain];
    }
}

- (NSMutableArray *)personsPresent
{
    return _personsPresent;
}

- (void)setPersonsPresent:(NSMutableArray *)aPersonsPresent
{
    if (_personsPresent != aPersonsPresent)
    {
        [self stopObservingPersonsPresent];
        [_personsPresent release];
        _personsPresent = [aPersonsPresent retain];
        [self startObservingPersonsPresent];
    }
}

- (void)addToPersonsPresent:(id)personsPresentObject
{
    
    [[self personsPresent] addObject:personsPresentObject];
    
    [personsPresentObject addObserver:self
                           forKeyPath:hourlyRateKeyPath
                              options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                              context:NULL];
}

- (void)removeFromPersonsPresent:(id)personsPresentObject
{
    [[self personsPresent] removeObject:personsPresentObject];
    
    [personsPresentObject removeObserver:self forKeyPath:hourlyRateKeyPath];
}

- (void)removeObjectFromPersonsPresentAtIndex:(NSUInteger)idx
{
    [[self personsPresent] removeObjectAtIndex:idx];
}

- (void)insertObject:(id)anObject inPersonsPresentAtIndex:(NSUInteger)idx
{
    [[self personsPresent] insertObject:anObject atIndex:idx];
}

- (void)insertPersonsPresent:(NSArray *)aPerson atIndexes:(NSIndexSet *)idx
{
    [[self personsPresent] insertObjects:aPerson atIndexes:idx];
}

- (void)removePersonsPresentAtIndexes:(NSIndexSet *)idx
{
    [[self personsPresent] removeObjectsAtIndexes:idx];
}

- (NSUInteger)countOfPersonsPresent
{
    return [[self personsPresent] count];
}

- (NSUInteger)elapsedSeconds
{
    NSUInteger seconds = 0;
    
    // check to see if endingTime is set yet
    if (![self endingTime])
        seconds = -1 * [[self startingTime] timeIntervalSinceNow];
    else
        seconds = [[self endingTime] timeIntervalSinceDate:[self startingTime]];
    
    return seconds;
}

- (double)elapsedHours
{
    return [self elapsedSeconds] / 3600.;
}

- (NSString *)elapsedTimeDisplayString
{
    NSUInteger elapsedSeconds = [self elapsedSeconds];
    NSUInteger hours, minutes, seconds;
    
    hours = elapsedSeconds / 3600;
    minutes = (elapsedSeconds / 60) % 60;
    seconds = elapsedSeconds % 60;
    
    return [NSString stringWithFormat:@"%0ld:%02ld:%02ld",hours,minutes,seconds];
}

- (NSNumber *)accruedCost
{
    double costs = 0.;
    costs = [self elapsedHours] * [[self totalBillingRate] doubleValue];
    
    return [NSNumber numberWithDouble:costs];
}

- (NSNumber *)totalBillingRate
{
    double billingRate = 0.;
    
    for (Person *p in self.personsPresent)
        billingRate += [[p hourlyRate] doubleValue];
    
    return [NSNumber numberWithDouble:billingRate];
}

- (BOOL)canStart
{    
    return ((self.countOfPersonsPresent > 1) &&
            !self.startingTime && !self.endingTime);
}

- (BOOL)canStop
{
    return (!self.endingTime && self.startingTime);
}

#pragma mark - Preset Meetings

+ (Meeting *)meetingWithStooges
{
    // Moe, Larry, Curly
    Meeting *stooges = [[[Meeting alloc] init] autorelease];
    
    [[stooges personsPresent] addObject:[Person personWithName:@"Moe" hourlyRate:@10]];
    [[stooges personsPresent] addObject:[Person personWithName:@"Larry" hourlyRate:@20]];
    [[stooges personsPresent] addObject:[Person personWithName:@"Curly" hourlyRate:@30]];
    
    [stooges startObservingPersonsPresent];
    
    return stooges;
}

+ (Meeting *)meetingWithCaptains
{
    // Archer, Kirk, Janeway, Sisko, Picard, April, Pike, Decker, Spock
    Meeting *captains = [[[Meeting alloc] init] autorelease];
    
    [[captains personsPresent] addObject:[Person personWithName:@"Archer" hourlyRate:@10]];
    [[captains personsPresent] addObject:[Person personWithName:@"Kirk" hourlyRate:@20]];
    [[captains personsPresent] addObject:[Person personWithName:@"Janeway" hourlyRate:@30]];
    [[captains personsPresent] addObject:[Person personWithName:@"Sisko" hourlyRate:@40]];
    [[captains personsPresent] addObject:[Person personWithName:@"Picard" hourlyRate:@50]];
    [[captains personsPresent] addObject:[Person personWithName:@"April" hourlyRate:@60]];
    [[captains personsPresent] addObject:[Person personWithName:@"Pike" hourlyRate:@70]];
    [[captains personsPresent] addObject:[Person personWithName:@"Decker" hourlyRate:@80]];
    [[captains personsPresent] addObject:[Person personWithName:@"Spock" hourlyRate:@90]];
    
    [captains startObservingPersonsPresent];
    
    return captains;
}

+ (Meeting *)meetingWithMarxBrothers
{
    // Chico, Harpo, Groucho, Zeppo, Gummo
    Meeting *marxBrothers = [[[Meeting alloc] init] autorelease];
    
    [[marxBrothers personsPresent] addObject:[Person personWithName:@"Chico Marx" hourlyRate:@10]];
    [[marxBrothers personsPresent] addObject:[Person personWithName:@"Harpo Marx" hourlyRate:@20]];
    [[marxBrothers personsPresent] addObject:[Person personWithName:@"Groucho Marx" hourlyRate:@30]];
    [[marxBrothers personsPresent] addObject:[Person personWithName:@"Zeppo Marx" hourlyRate:@40]];
    [[marxBrothers personsPresent] addObject:[Person personWithName:@"Gummo Marx" hourlyRate:@50]];
    
    [marxBrothers startObservingPersonsPresent];
    
    return marxBrothers;
}

#pragma mark - KVO

+ (NSSet *)keyPathsForValuesAffectingCanStart
{
    return [NSSet setWithObjects:personsPresentKeyPath, startingTimeKeyPath, endingTimeKeyPath, nil];
}

+ (NSSet *)keyPathsForValuesAffectingCanStop
{
    return [NSSet setWithObjects:startingTimeKeyPath, endingTimeKeyPath, nil];
}

- (void)startObservingPersonsPresent
{
    for (Person *p in [self personsPresent])
    {   // add observer to each hourlyRate
        [p addObserver:self
            forKeyPath:hourlyRateKeyPath
               options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
               context:NULL];
    }
}

- (void)stopObservingPersonsPresent
{
    for (Person *p in [self personsPresent])
    {   // remove the observer on each hourlyRate
        [p removeObserver:self forKeyPath:hourlyRateKeyPath];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    //NSLog(@"Meeting OVFKP\nKEYPATH: %@\nCHANGE:\n%@\nKINDKEY: %@", keyPath, change, change[NSKeyValueChangeKindKey]);
    
    if ([keyPath isEqualToString:hourlyRateKeyPath])
    {   // an hourly rate has been changed
        [self willChangeValueForKey:@"totalBillingRate"];
        [self didChangeValueForKey:@"totalBillingRate"];
    }
    else if ([keyPath isEqualToString:personsPresentKeyPath])
    {   // figure out what happened to personsPresent array
        switch ([change[NSKeyValueChangeKindKey] integerValue])
        {   // not concerned with this change yet
            case NSKeyValueChangeSetting:
                break;
                // insertion and removal need to have an observer added or removed based on if it is new or old
            case NSKeyValueChangeInsertion:
            case NSKeyValueChangeRemoval:
                for (Person *p in change[NSKeyValueChangeNewKey])
                {
                    [p addObserver:self
                        forKeyPath:hourlyRateKeyPath
                           options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                           context:NULL];
                }
                for (Person *p in change[NSKeyValueChangeOldKey])
                {
                    [p removeObserver:self forKeyPath:hourlyRateKeyPath];
                }
                break;
                // not concerned with replacements yet
            case NSKeyValueChangeReplacement:
                break;
        }
        // update hourly rate incase it was changed
        [self willChangeValueForKey:@"totalBillingRate"];
        [self didChangeValueForKey:@"totalBillingRate"];
    }
}

@end
