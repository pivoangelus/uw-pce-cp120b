//
//  AppDelegate.h
//  IB Calisthenics
//
//  Created by Justin Andros on 10/7/14.
//
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (weak) IBOutlet NSSpeechSynthesizer *speechSyth;

- (IBAction)helloButton:(id)sender;
- (IBAction)goodbyeButton:(id)sender;
- (IBAction)copyButton:(id)sender;
- (IBAction)segmentedButtons:(id)sender;
- (IBAction)nowButton:(id)sender;
- (IBAction)squareRootSlider:(id)sender;
- (IBAction)seasonMatrix:(id)sender;
- (IBAction)voiceSelectMatrix:(id)sender;
- (IBAction)voiceSpeedSlider:(id)sender;
- (IBAction)speakButton:(id)sender;
- (IBAction)shushButton:(id)sender;

@end
