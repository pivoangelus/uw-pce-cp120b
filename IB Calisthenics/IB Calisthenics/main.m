//
//  main.m
//  IB Calisthenics
//
//  Created by Justin Andros on 10/7/14.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
