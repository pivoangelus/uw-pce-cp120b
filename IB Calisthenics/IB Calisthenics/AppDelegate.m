//
//  AppDelegate.m
//  IB Calisthenics
//
//  Created by Justin Andros on 10/7/14.
//
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *helloLabel;
@property (weak) IBOutlet NSTextField *helloTextField;
@property (weak) IBOutlet NSTextField *segmentedLabel;
@property (weak) IBOutlet NSTextField *dateLabel;
@property (weak) IBOutlet NSTextField *squareRootAnswerLabel;
@property (weak) IBOutlet NSTextField *squareRootSliderLabel;
@property (weak) IBOutlet NSTextField *seasonLabel;
@property (weak) IBOutlet NSTextField *voiceTextField;
@property (weak) IBOutlet NSSlider *speechSlider;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [self.speechSyth setVoice:@"com.apple.speech.synthesis.voice.Victoria"];
    [self.speechSlider setFloatValue:self.speechSyth.rate];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
}

// HELLO GOODBYE
- (IBAction)helloButton:(id)sender {
    self.helloLabel.stringValue = @"Hello world";
}

- (IBAction)goodbyeButton:(id)sender {
    self.helloLabel.stringValue = @"Goodbye";
}

- (IBAction)copyButton:(id)sender {
    self.helloLabel.stringValue = [self.helloTextField stringValue];
}

// ZERO,ONE,TWO
- (IBAction)segmentedButtons:(NSSegmentedCell *)sender {
    switch ([sender selectedSegment]) {
        case 0:
            self.segmentedLabel.stringValue = @"0: Zero";
            break;
        case 1:
            self.segmentedLabel.stringValue = @"1: One";
            break;
        case 2:
            self.segmentedLabel.stringValue = @"2: Two";
            break;
    }
}

// DATE
- (IBAction)nowButton:(id)sender {
    self.dateLabel.objectValue = [NSDate date];
}

// SQUARE ROOT SLIDER
- (IBAction)squareRootSlider:(NSSlider *)sender {
    float squareRootValue = [sender floatValue];
    self.squareRootSliderLabel.floatValue = squareRootValue;
    self.squareRootAnswerLabel.floatValue = sqrtf(squareRootValue);
}

// SEASON STARTS ...
- (IBAction)seasonMatrix:(NSMatrix *)sender {
    switch ([sender selectedRow]) {
        case 0:
            self.seasonLabel.stringValue = @"Decemeber";
            break;
        case 1:
            self.seasonLabel.stringValue = @"March";
            break;
        case 2:
            self.seasonLabel.stringValue = @"June";
            break;
        case 3:
            self.seasonLabel.stringValue = @"September";
            break;
    }
}

// VOICE!
- (IBAction)voiceSelectMatrix:(NSSegmentedCell *)sender {
    switch ([sender selectedSegment]) {
        case 0:
            [self.speechSyth setVoice:@"com.apple.speech.synthesis.voice.Alex"];
            break;
        case 1:
            [self.speechSyth setVoice:@"com.apple.speech.synthesis.voice.Victoria"];
            break;
        case 2:
            [self.speechSyth setVoice:@"com.apple.speech.synthesis.voice.Cellos"];
            break;
        case 3:
            [self.speechSyth setVoice:@"com.apple.speech.synthesis.voice.Trinoids"];
            break;
    }
    
    [self.speechSlider setFloatValue:self.speechSyth.rate];
}

- (IBAction)voiceSpeedSlider:(NSSlider *)sender {
    [self.speechSyth setRate:[sender floatValue]];
}

- (IBAction)speakButton:(id)sender {
    [self.speechSyth startSpeakingString:[self.voiceTextField stringValue]];
}

- (IBAction)shushButton:(id)sender {
    [self.speechSyth stopSpeaking];
}

@end
